'use strict';

function scrollBottom()
{
    var element = $('#message-container');
    element.scrollTop(element.prop('scrollHeight'));
}

$(() => {
    const socket = io();
    
    function sendMsg()
    {
        var txt = $('#msg').val();
        if(txt)
        {
            socket.emit('chat', txt);
        }
        $('#msg').val("");  
    }

    socket.on('init', (msg) => {
        $('#messages').empty();
        $('#messages').append(msg);
        scrollBottom();
    });

    socket.on('chat', (msg) => {
        console.log("message received!!!!!")
        console.log(msg);
        $('#messages').append(msg);
        scrollBottom();
    });

    $('#send').on('click', () => {
        sendMsg();
    });
    
    $('#msg').on('keyup', (key) => {
        if (key.key == "Enter")
        {
            sendMsg();
        }
    });
});
