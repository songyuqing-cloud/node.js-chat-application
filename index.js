'use strict';

const port = 3000;
const host = 'localhost';

const sqlite3 = require('sqlite3');
const fs = require('fs');
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const io = require('socket.io')(server, {
    serveClient: false
});

let db = new sqlite3.Database('./nodeChat.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err)
    {
        return console.error(err.message);
    }
    console.log('Connected to the nodeChat.db database.');
});

if (!fs.existsSync('./messages.txt'))
{
    //TODO: Catch errors
    fs.writeFileSync('./messages.txt', '');
}

// Replace with database?
var messagesFile = fs.createWriteStream('./messages.txt', {flags:'a'});

app.use(express.static(__dirname + '/public/'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});

//app.use(express.json());
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.post('/new_user', (req, res) => {
    //TODO: DO SOMETHING WHEN AN ERROR OCCURS
    //TODO: REDIRECT AFTER ADDING A NEW USER
    //TODO: HASH PASSWORDS
    //TODO: REMOVE CONSOLE.LOG
    console.log(req.body);
    db.run(`INSERT INTO users(username, password) VALUES(?, ?)`, [req.body.username, req.body.password], function(err) {
        if (err) {
          return console.log(err.message);
        }
        // get the last insert id
        console.log(`A row has been inserted with rowid ${this.lastID}`);
      });
});

io.on('connection', (socket) => {
    console.log('A user has connected');

    socket.emit('init', fs.readFileSync('./messages.txt', {encoding: 'utf-8', flag: 'r'}));

    socket.on('chat', (msg) => {
        console.log(msg);

        // Check for errors?    
        messagesFile.write(msg + '<br />\n');
        io.emit('chat', msg + '<br />');
    });



    socket.on('disconnect', () => {
        console.log('A user has disconnected');
    })
});

// Find what this is doing:
server.listen(port, () => {
    console.log('server listening on :' + port);
});


/*
db.close((err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Close the database connection.');
});
*/